/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stm32f411e_discovery_accelerometer.h"
#include "tim.h"
#include "arm_math.h"
#include "math.h"
#include "queue.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
extern TIM_HandleTypeDef htim4;

/* USER CODE END Variables */
/* Definitions for defaultTask */
osThreadId_t defaultTaskHandle;
const osThreadAttr_t defaultTask_attributes = {
  .name = "defaultTask",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityNormal,
};
/* Definitions for Sample_Task */
osThreadId_t Sample_TaskHandle;
const osThreadAttr_t Sample_Task_attributes = {
  .name = "Sample_Task",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityRealtime6,
};
/* Definitions for Process_Task */
osThreadId_t Process_TaskHandle;
const osThreadAttr_t Process_Task_attributes = {
  .name = "Process_Task",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityRealtime5,
};
/* Definitions for LED_TASK */
osThreadId_t LED_TASKHandle;
const osThreadAttr_t LED_TASK_attributes = {
  .name = "LED_TASK",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityRealtime4,
};
/* Definitions for Toggle_Task */
osThreadId_t Toggle_TaskHandle;
const osThreadAttr_t Toggle_Task_attributes = {
  .name = "Toggle_Task",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityRealtime3,
};
/* Definitions for Button_Task */
osThreadId_t Button_TaskHandle;
const osThreadAttr_t Button_Task_attributes = {
  .name = "Button_Task",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityRealtime7,
};
/* Definitions for GeneralQueue */
osMessageQueueId_t GeneralQueueHandle;
const osMessageQueueAttr_t GeneralQueue_attributes = {
  .name = "GeneralQueue"
};

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */
osMessageQueueId_t SamplesQueueHandle;
const osMessageQueueAttr_t SamplesQueue_attributes = {
  .name = "SamplesQueue"
};

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin);

/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void *argument);
void SampleTask(void *argument);
void ProcessTask(void *argument);
void LEDTask(void *argument);
void ToggleTask(void *argument);
void Buttontask(void *argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* Hook prototypes */
void vApplicationIdleHook(void);

/* USER CODE BEGIN 2 */
void vApplicationIdleHook( void )
{
   /* vApplicationIdleHook() will only be called if configUSE_IDLE_HOOK is set
   to 1 in FreeRTOSConfig.h. It will be called on each iteration of the idle
   task. It is essential that code added to this hook function never attempts
   to block in any way (for example, call xQueueReceive() with a block time
   specified, or call vTaskDelay()). If the application makes use of the
   vTaskDelete() API function (as this demo application does) then it is also
   important that vApplicationIdleHook() is permitted to return to its calling
   function, because it is the responsibility of the idle task to clean up
   memory allocated by the kernel to any task that has since been deleted. */
}
/* USER CODE END 2 */

/* USER CODE BEGIN PREPOSTSLEEP */
__weak void PreSleepProcessing(uint32_t ulExpectedIdleTime)
{
/* place for user code */
}

__weak void PostSleepProcessing(uint32_t ulExpectedIdleTime)
{
/* place for user code */
}
/* USER CODE END PREPOSTSLEEP */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the queue(s) */
  /* creation of GeneralQueue */
  GeneralQueueHandle = osMessageQueueNew (1, sizeof(int), &GeneralQueue_attributes);

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  SamplesQueueHandle = osMessageQueueNew (30, 3*sizeof(int16_t*), &SamplesQueue_attributes);
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of defaultTask */
  defaultTaskHandle = osThreadNew(StartDefaultTask, NULL, &defaultTask_attributes);

  /* creation of Sample_Task */
  Sample_TaskHandle = osThreadNew(SampleTask, NULL, &Sample_Task_attributes);

  /* creation of Process_Task */
  Process_TaskHandle = osThreadNew(ProcessTask, NULL, &Process_Task_attributes);

  /* creation of LED_TASK */
  LED_TASKHandle = osThreadNew(LEDTask, NULL, &LED_TASK_attributes);

  /* creation of Toggle_Task */
  Toggle_TaskHandle = osThreadNew(ToggleTask, NULL, &Toggle_Task_attributes);

  /* creation of Button_Task */
  Button_TaskHandle = osThreadNew(Buttontask, NULL, &Button_Task_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_EVENTS */
  /* add events, ... */
  /* USER CODE END RTOS_EVENTS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void *argument)
{
  /* USER CODE BEGIN StartDefaultTask */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Header_SampleTask */
/**
* @brief Function implementing the Sample_Task thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_SampleTask */
void SampleTask(void *argument)
{
  /* USER CODE BEGIN SampleTask */
  static int samples;

  int16_t* p_acc;
  int16_t acc[3];
  p_acc= &acc[0];

  /* Infinite loop */
  for(;;)
  {
	  BSP_ACCELERO_GetXYZ(p_acc);

	  samples++;

	  xQueueSend(SamplesQueueHandle, p_acc, portMAX_DELAY);
	  if(samples>=19){
		  samples = 0;
		  vTaskResume(Process_TaskHandle);
	  }
      osDelay(5);
  }
  /* USER CODE END SampleTask */
}

/* USER CODE BEGIN Header_ProcessTask */
/**
* @brief Function implementing the Process_Task thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_ProcessTask */
void ProcessTask(void *argument)
{
  /* USER CODE BEGIN ProcessTask */
  int16_t* p_acc;
  int16_t acc[3];
  p_acc= &acc[0];

  int shake;

  float32_t pow_module;
  float32_t module = 0;
  float32_t *abs_module = &module;

  int16_t x = 0;
  int16_t y = 0;
  int16_t z = 0;

  float32_t min = 0xFFFFFFFF;
  float32_t max = 0;
  /* Infinite loop */
  for(;;)
  {
	min = 0xFFFFFFFF; /*reset min & max*/
	max = 0;

	for (char i = 0; i < 20 ; i++){
		xQueueReceive(SamplesQueueHandle, p_acc, portMAX_DELAY);

		x = acc[0];
		y = acc[1];
		z = acc[2];

		pow_module = abs((x*x)) + abs((y*y)) + abs((z*z));

		arm_sqrt_f32(pow_module, abs_module);

		if(max < (int) *abs_module)
			max = (int) *abs_module;
		if(min > (int) *abs_module)
			min = (int) *abs_module;
	}
	shake = (int) max-min;

	xQueueSend(GeneralQueueHandle, &shake, portMAX_DELAY);

	vTaskResume(LED_TASKHandle);
	vTaskSuspend(NULL);
  }
  /* USER CODE END ProcessTask */
}

/* USER CODE BEGIN Header_LEDTask */
/**
* @brief Function implementing the LED_TASK thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_LEDTask */
void LEDTask(void *argument)
{
  /* USER CODE BEGIN LEDTask */
  int shake = 0;
  /* Infinite loop */
  for(;;)
  {
	xQueueReceive(GeneralQueueHandle, &shake, portMAX_DELAY);
	if(shake > 3000){
		__HAL_TIM_SET_COMPARE(&htim4, TIM_CHANNEL_1, 90);
	}
	if(shake <= 3000 && shake > 2400){
		__HAL_TIM_SET_COMPARE(&htim4, TIM_CHANNEL_1, 50);
	}
	if(shake <= 2400){
		__HAL_TIM_SET_COMPARE(&htim4, TIM_CHANNEL_1, 15);
	}
	vTaskSuspend(NULL);
  }
  /* USER CODE END LEDTask */
}

/* USER CODE BEGIN Header_ToggleTask */
/**
* @brief Function implementing the Toggle_Task thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_ToggleTask */
void ToggleTask(void *argument)
{
  /* USER CODE BEGIN ToggleTask */
  static uint32_t miliseconds;
  /* Infinite loop */
  for(;;)
  {
	HAL_GPIO_TogglePin(GPIOD,GPIO_PIN_15);

	miliseconds = HAL_GetTick();
	UNUSED(miliseconds);

    osDelay(1000);
  }
  /* USER CODE END ToggleTask */
}

/* USER CODE BEGIN Header_Buttontask */
/**
* @brief Function implementing the Button_Task thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_Buttontask */
void Buttontask(void *argument)
{
  /* USER CODE BEGIN Buttontask */
  char on;
  /* Infinite loop */
  for(;;)
  {
	if(on){
		HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_1);

		vTaskSuspend(Process_TaskHandle);
		vTaskSuspend(LED_TASKHandle);

		vTaskResume(Toggle_TaskHandle);
		vTaskResume(Sample_TaskHandle);

		on = 0;
	}else{
		HAL_TIM_PWM_Stop(&htim4, TIM_CHANNEL_1);

		vTaskSuspend(Sample_TaskHandle);
		vTaskSuspend(Process_TaskHandle);
		vTaskSuspend(LED_TASKHandle);

		on = 1;
  	}
	vTaskSuspend(NULL);
  }
  /* USER CODE END Buttontask */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
  if(GPIO_Pin == BUTTON_GPIO_Pin){
	  xTaskResumeFromISR(Button_TaskHandle);
  }
}
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
